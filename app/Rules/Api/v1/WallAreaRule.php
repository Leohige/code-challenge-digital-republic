<?php
 
namespace App\Rules\Api\v1;
 
use Illuminate\Contracts\Validation\Rule;
 
class WallAreaRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $param
     */
    public function __construct(array $params = ['min' => 1, 'max' => 50])
    {
        $this->params = $params;
    }

    /**
     * Determine if the area value is between the minimum and maximum range.
     *
     * @param  string  $attribute
     * @param  array  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!array_key_exists('width', $value) || !array_key_exists('height', $value) ||
            !is_numeric($value['width']) || !is_numeric($value['height'])) {
            return true;
        }

        $area = $value['width'] * $value['height'];

        return ($area <= $this->params['max']) && ($area >= $this->params['min']);
    }
 
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute total area must be greater than 1 and less than 50.';
    }
}
