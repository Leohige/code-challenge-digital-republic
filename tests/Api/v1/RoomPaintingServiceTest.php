<?php

use App\Services\Api\v1\RoomPaintingService;

class RoomPaintingServiceTest extends TestCase
{
    public function testShouldCalculateCorrectPaintCans()
    {
        $roomPaintingService = new RoomPaintingService();

        $walls = [
            ['width' => 4, 'height' => 5, 'windows' => 1, 'doors' => 1],
            ['width' => 3, 'height' => 3, 'windows' => 0, 'doors' => 0],
            ['width' => 7, 'height' => 4, 'windows' => 1, 'doors' => 0],
            ['width' => 5, 'height' => 7, 'windows' => 1, 'doors' => 1],
        ];
        $expected = [
            '0,5 L' => 4,
            '2,5 L' => 0,
            '3,6 L' => 4,
            '18 L'  => 0
        ];

        $result = $roomPaintingService->calculatePaintCans($walls);

        $this->assertEquals($expected, $result);
    }

    public function testShouldCalculateCorrectWallArea()
    {
        $roomPaintingService = new RoomPaintingService();

        $wall = [
            'width' => 4, 'height' => 7, 'windows' => 3, 'doors' => 2
        ];
        $expected = 17.76;

        $result = $roomPaintingService->calculateWallArea($wall);

        $this->assertEquals($expected, $result);
    }

    public function testShouldCalculateCorrectTotalArea()
    {
        $roomPaintingService = new RoomPaintingService();

        $walls = [
            ['width' => 7, 'height' => 5, 'windows' => 3, 'doors' => 0],
            ['width' => 3, 'height' => 7, 'windows' => 2, 'doors' => 2],
            ['width' => 5, 'height' => 3, 'windows' => 0, 'doors' => 3],
            ['width' => 6, 'height' => 4, 'windows' => 4, 'doors' => 2],
        ];
        $expected = 62.76;

        $result = $roomPaintingService->calculateTotalArea($walls);

        $this->assertEquals($expected, $result);
    }

    public function testShouldGetCorrectPaintCans()
    {
        $roomPaintingService = new RoomPaintingService();

        $area = 10.52;
        $expected = [
            '0,5 L' => 5,
            '2,5 L' => 0,
            '3,6 L' => 0,
            '18 L'  => 0
        ];

        $result = $roomPaintingService->getPaintCans($area);

        $this->assertEquals($expected, $result);
    }
}
