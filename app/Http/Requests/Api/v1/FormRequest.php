<?php

namespace App\Http\Requests\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

class FormRequest
{
    use ProvidesConvenienceMethods;

    public Request $req;

    public function __construct(Request $request, array $messages = [], array $customAttributes = [])
    {
        $this->req = $request;

        $this->validate($this->req, $this->rules(), $messages, $customAttributes);
    }

    public function get(string $key, $default = null)
    {
        return $this->req->get($key, $default);
    }

    public function only(array $attributes = [])
    {
        return $this->req->only($attributes);
    }

    protected function rules()
    {
        return [];
    }
}
