# CODE CHALLENGE DIGITAL REPUBLIC

This project has been created as part of the backend developer job application process at Digital Republic.

It was developed using [Lumen](https://lumen.laravel.com/docs/8.x) 8.1.2.

## 💿 Cloning

```shell
git@gitlab.com:Leohige/code-challenge-digital-republic.git
```

## 🚀 Running

You will need [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) to run.

```shell
docker compose up api --build
```

## ⚙️ Tests

```shell
docker compose up tests --build
```

## 📦 Endpoint

`POST` _http://localhost:3000/api/v1/room-painting_

## 📁 Body

```json
{
    "walls": [
        {
			"width": 4,
			"height": 4,
			"windows": 2,
			"doors": 1
		},
		{
			"width": 4,
			"height": 10,
			"windows": 2,
			"doors": 2
		},
		{
			"width": 4,
			"height": 10,
			"windows": 2,
			"doors": 2
		},
		{
			"width": 4,
			"height": 10,
			"windows": 2,
			"doors": 2
		}
    ]
}
```

## 🌐 Curl Request
```shell
curl --request POST \
     --url http://localhost:8080/api/v1/room-painting \
     --header 'Content-Type: application/json' \
     --data \
     '{
        "walls": [
            {
                "width": 4,
                "height": 4,
                "windows": 2,
                "doors": 1
            },
            {
                "width": 4,
                "height": 10,
                "windows": 2,
                "doors": 2
            },
            {
                "width": 4,
                "height": 10,
                "windows": 2,
                "doors": 2
            },
            {
                "width": 4,
                "height": 10,
                "windows": 2,
                "doors": 2
            }
        ]
    }'
```

## ✅ Response (success)
```json
{
	"paint cans": {
		"0,5 L": 2,
		"2,5 L": 1,
		"3,6 L": 0,
		"18 L": 1
	}
}
```

## ❌ Response (fail)
```json
{
	"walls.0.height": [
		"The walls.0.height must be at least 2.2 when there is a door."
	],
	"walls.0": [
		"The walls.0 doors and windows total area must be less than 50% of the wall total area."
	],
	"walls.1": [
		"The walls.1 total area must be greater than 1 and less than 50."
	],
	"walls.2": [
		"The walls.2 doors and windows total area must be less than 50% of the wall total area.",
		"The walls.2 total area must be greater than 1 and less than 50."
	]
}
```
