<?php
 
namespace App\Rules\Api\v1;
 
use Illuminate\Contracts\Validation\Rule;
 
class FreeWallAreaRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param $param
     */
    public function __construct(array $params = ['min' => 1])
    {
        $this->params = $params;
    }

    /**
     * Determine if the area value is equal to or greater than the minimum value expressed as a percentage.
     *
     * @param  string  $attribute
     * @param  array  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!array_key_exists('width', $value) || !array_key_exists('height', $value) ||
            !array_key_exists('windows', $value) || !array_key_exists('doors', $value) ||
            !is_numeric($value['width']) || !is_numeric($value['height']) || 
            !is_numeric($value['windows']) || !is_numeric($value['doors']) || 
            !array_key_exists('min', $this->params)) {
            return true;
        }

        $totalArea = $value['width'] * $value['height'];
        $occupiedArea = ($value['windows'] * 2.4) + ($value['doors'] * 1.52);
        $freeArea = $totalArea - $occupiedArea;
        $minFreeArea = $totalArea * ($this->params['min'] / 100);

        return ($freeArea >= $minFreeArea);
    }
 
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute doors and windows total area must be less than 50% of the wall total area.';
    }
}