<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\Api\v1\RoomPaintingService;
use App\Http\Requests\Api\v1\CalculatePaintCansRequest;
use Illuminate\Http\Request;


class RoomPaintingController extends Controller
{
    private RoomPaintingService $roomPaintingService;

    public function __construct(RoomPaintingService $roomPaintingService)
    {
        $this->roomPaintingService = $roomPaintingService;
    }

    public function calculatePaintCans(CalculatePaintCansRequest $request)
    {
        $walls = $request->get('walls');
        $paintCans = $this->roomPaintingService->calculatePaintCans($walls);

        return response()->json(['paint cans' => $paintCans], 200);
    }
}
