<?php

namespace App\Http\Requests\Api\v1;

use App\Http\Requests\Api\v1\FormRequest;
use App\Rules\Api\v1\WallAreaRule;
use App\Rules\Api\v1\FreeWallAreaRule;
use App\Rules\Api\v1\MinWallHeightRule;

class CalculatePaintCansRequest extends FormRequest
{
    public function rules()
    {
        return [
            'walls'           => 'required|array|size:4',
            'walls.*.width'   => 'required|numeric',
            'walls.*.windows' => 'required|numeric',
            'walls.*.doors'   => 'required|numeric',
            'walls.*.height'  => [
                'required', 
                'numeric', 
                new MinWallHeightRule(['min' => 2.20])
            ],
            'walls.*'         => [
                new FreeWallAreaRule(['min' => 50]), 
                new WallAreaRule(['min' => 1, 'max' => 50])
            ],
        ];
    }
}
