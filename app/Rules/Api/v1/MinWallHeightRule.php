<?php
 
namespace App\Rules\Api\v1;

use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;
 
class MinWallHeightRule implements DataAwareRule, Rule
{
    /**
     * All of the data under validation.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Determine if the wall height has a minimum value of 2.20 when the number of doors is greater than 0.
     *
     * @param  string  $attribute
     * @param  array  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $index = explode('.', $attribute)[1];
        $wall = $this->data['walls'][$index];

        if (!array_key_exists('doors', $wall)) {
            return true;
        }

        $doors = $wall['doors'];
        $height = $value;

        return !$doors || ($doors > 0 && $height >= 2.2);
    }
 
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be at least 2.2 when there is a door.';
    }

    /**
     * Set the data under validation.
     *
     * @param  array  $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
 
        return $this;
    }
}
