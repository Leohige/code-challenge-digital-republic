<?php

namespace App\Services\Api\v1;

class RoomPaintingService
{
    public function calculatePaintCans(array $walls = [])
    {
        $area = $this->calculateTotalArea($walls);
        return $this->getPaintCans($area);
    }

    public function calculateWallArea(array $wall = ['width' => 0, 'height' => 0, 'windows' => 0, 'doors' => 0])
    {
        $wallArea = $wall['width'] * $wall['height'];
        $windowsArea = $wall['windows'] * 2.4;
        $doorsArea = $wall['doors'] * 1.52;
        
        return $wallArea - $windowsArea - $doorsArea;
    }

    public function calculateTotalArea(array $walls = [])
    {
        $area = 0;

        foreach ($walls as &$wall) {
            $area += $this->calculateWallArea($wall);
        }

        return $area;
    }

    public function getPaintCans(float $area)
    {
        $paintCans = [
            '0,5 L' => 0,
            '2,5 L' => 0,
            '3,6 L' => 0,
            '18 L'  => 0
        ];

        while ($area > 0) {
            switch ($area) {
                case ($area >= 90):
                    $paintCans['18 L'] += 1;
                    $area -= 90;
                    break;
                case ($area >= 18):
                    $paintCans['3,6 L'] += 1;
                    $area -= 18;
                    break;
                case ($area >= 12.5):
                    $paintCans['2,5 L'] += 1;
                    $area -= 12.5;
                    break;
                case ($area >= 2.5):
                default:
                    $paintCans['0,5 L'] += 1;
                    $area -= 2.5;
                    break;
            }
        }

        return $paintCans;
    }
}
