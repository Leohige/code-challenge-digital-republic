<?php

use App\Services\Api\v1\RoomPaintingService;

class RoomPaintingTest extends TestCase
{
    public function testShouldCalculateCorrectPaintCans()
    {
        $parameters = [
            'walls' => [
                [
                    'width'   => 4,
                    'height'  => 5,
                    'windows' => 2,
                    'doors'   => 1
                ],
                [
                    'width'   => 6,
                    'height'  => 5,
                    'windows' => 2,
                    'doors'   => 1
                ],
                [
                    'width'   => 8,
                    'height'  => 5,
                    'windows' => 1,
                    'doors'   => 0
                ],
                [
                    'width'   => 9,
                    'height'  => 5,
                    'windows' => 3,
                    'doors'   => 0
                ]
            ],
        ];

        $expected = [
            'paint cans' => [
                '0,5 L' => 2,
                '2,5 L' => 0,
                '3,6 L' => 1,
                '18 L'  => 1
            ]
        ];

        $response = $this->call('POST', 'api/v1/room-painting', $parameters);

        $this->assertEquals(200, $response->status());
        $this->assertEquals($expected, $response->json());
    }

    public function testShouldGetValidationsErrors()
    {
        $parameters = [
            'walls' => [
                [
                    'width'   => 4,
                    'height'  => 2,
                    'windows' => 2,
                    'doors'   => 1
                ],
                [
                    'width'   => 10,
                    'height'  => 10,
                    'windows' => 2,
                    'doors'   => 2
                ],
                [
                    'width'   => 0,
                    'height'  => 5,
                    'doors'   => 0
                ]
            ],
        ];

        $expected = [
            'walls',
            'walls.0',
            'walls.0.height',
            'walls.1',
            'walls.2',
            'walls.2.windows'
        ];

        $response = $this->call('POST', 'api/v1/room-painting', $parameters);

        $this->assertEquals(422, $response->status());
        $response->assertJsonValidationErrors($expected, $responseKey = null);
    }
}
