FROM alpine:3.13

RUN apk --no-cache add \
php7 \
php7-fpm \
php7-pdo \
php7-mbstring \
php7-openssl \
php7-json \
php7-dom \
curl \
php7-curl \
php7-tokenizer \
php7-phar \
php7-xml \
php7-xmlwriter

# if need composer to update plugin / vendor used
RUN php7 -r "copy('http://getcomposer.org/installer', 'composer-setup.php');" && \
php7 composer-setup.php --install-dir=/usr/bin --filename=composer && \
php7 -r "unlink('composer-setup.php');"

# copy composer.json to /src
COPY composer.json /src/composer.json
COPY tests /src/tests
WORKDIR /src

RUN composer update

# copy all of the file in folder to /src
COPY . /src
WORKDIR /src
